package com.diksha.tst.testscripts;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@Listeners(com.dikshatech.tst.configuration.TestNGListeners.class)
public class MFSapisvalidator extends TestBase {

	public static final Logger logger = Logger.getLogger(MFSapisvalidator.class);
	static String sessionid;
	String credittoken;

	@Test(enabled = true, priority = 8)
	public static void generatesessionID() {
		exlogger = report.startTest("MFS API Validation Report");
		test = exlogger.assignCategory("MFS API Validation");
		Response resp = RestAssured.given().formParam("MobileNumber", "254708086131")
				.get("https://test.equitybankgroup.com:8443/mfmbs/test/session");
		switch (resp.getStatusCode()) {
		case 504:
			logger.error("MFS generate Session Response is throwing a Gateway Timeout/Service Callout error:  "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"MFS generate Session Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			break;
		case 500:
			logger.error("MFS generate Session Response is throwing an Internal Server error/Server side error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.UNKNOWN,
					"MFS generate Session Response is throwing an Internal Server error/Server side error:  "
							+ resp.getStatusLine());
			break;
		case 401:
			logger.error("MFS generate Session Response is throwing an Unauthorized error/invalid credentials: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"MFS generate Session Response is throwing an Unauthorized error/invalid credentials: "
							+ resp.getStatusLine());
			break;
		case 404:
			logger.error(
					"MFS generate Session Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"MFS generate Session Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			break;
		case 400:
			logger.error("MFS generate Session Response is throwing a Bad request/resource not found error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"MFS generate Session Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			break;
		default:
			logger.info("MFS generate Session Response status line is" + resp.getStatusLine());
			break;
		}
		Assert.assertEquals(resp.getStatusCode(), 200,
				"Expected MFS generate session ID status code not returned and the statusline is: "
						+ resp.getStatusLine());
		Assert.assertEquals(resp.body().asString().contains("sessionid"), true,
				"MFS generate sessionID Response body does not contain sessionid key in it and the response body is : "
						+ resp.body().asString());

		sessionid = resp.jsonPath().get("sessionid");
		if (sessionid != null && !sessionid.isEmpty()) {
			logger.info("Session ID for MFS API's is :  " + sessionid);
			logger.info("MFS Session ID Response Time : " + resp.getTime());
			exlogger.log(LogStatus.PASS, "Session ID for MFS API's  is :  " + sessionid
					+ " and MFS Session ID Response Time is : " + resp.getTime());
			exlogger.log(LogStatus.INFO, "MFS Session ID Success Response body :  " + resp.body().asString());
		} else {
			logger.error("MFS Session ID is either null or empty. " + resp.body().asString());
			exlogger.log(LogStatus.FAIL, "MFS Session ID is either null or empty. " + "[" + resp.body().asString() + "]"
					+ " and Response Time is : " + resp.getTime());
		}
	}

	@Test(enabled = true, priority = 9)
	public void getassociatedcards() {
		if (!sessionid.equalsIgnoreCase("null") && !sessionid.isEmpty()) {
			Map<String, Object> assocardppayload = new HashMap<>();
			assocardppayload.put("rid", "1");
			assocardppayload.put("pin", "1234");
			assocardppayload.put("msisdn", "0708086131");

			RequestSpecification req = RestAssured.given();
			req.header("Content-Type", "application/json");
			req.header("X_SESSION", sessionid);
			req.body(assocardppayload).when();
			Response resp = req.post("https://test.equitybankgroup.com:8443/mfmbs/getAssociatedCards");
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("MFS Associated Cards Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Associated Cards Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("MFS Associated Cards Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"MFS Associated Cards Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("MFS Associated Cards is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"MFS Associated Cards is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("MFS Associated Cards Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"MFS Associated Cards Response is throwing a Page/Server not found error: "
								+ resp.getStatusLine());
				break;
			case 400:
				logger.error("MFS Associated Cards Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Associated Cards Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("MFS Associated Cards Response status line is" + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected MFS Associated Cards status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("cards"), true,
					"MFS Associated Cards Response body does not contain Cards key in it.");
			String cardsobj = resp.jsonPath().getString("cards");
			if (cardsobj != null && !cardsobj.isEmpty()) {
				logger.info("MFS Associated Cards response is :  " + resp.body().asString());
				logger.info("MFS Associated Cards Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "MFS Associated Cards response is :  " + cardsobj
						+ " and MFS Session ID Response Time is : " + resp.getTime());
				JSONObject mfsproduct = new JSONObject(resp.body().asString());
				JSONArray array = mfsproduct.getJSONArray("cards");

				for (int i = 0; i < array.length(); i++) {
					String prodname = resp.jsonPath().getString("cards[" + i + "].productName");
					logger.info("Associated Cards product Name is - " + prodname);
					if (prodname.equalsIgnoreCase("VISA CARD")) {
						credittoken = resp.jsonPath().getString("cards[" + i + "].token");
						exlogger.log(LogStatus.INFO, "Creidt Token for " + prodname + "is : " + credittoken);
					}
				}
			} else {
				logger.error("MFS Associated Cards is either null or empty. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL, "MFS Associated Cards is either null or empty. " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}

		} else {
			logger.error("MFS Session ID is either null or empty.");
			exlogger.log(LogStatus.FAIL, "MFS Session ID is either null or empty.");
		}
	}

	@Test(enabled = true, priority = 10)
	public void mbankingtransOTP() {
		if (!sessionid.equalsIgnoreCase("null") && !sessionid.isEmpty()) {
			Map<String, Object> mtransotppayload = new HashMap<>();
			mtransotppayload.put("rid", "1");
			mtransotppayload.put("mobileNumber", "254764555330");
			mtransotppayload.put("accountNumber", "0350191765307");
			mtransotppayload.put("amount", "500");
			mtransotppayload.put("currency", "KES");
			mtransotppayload.put("service", "TRANSFERTOEQUITY");

			RequestSpecification req = RestAssured.given();
			req.header("Content-Type", "application/json");
			req.header("X_SESSION", sessionid);
			req.header("IMEI", "354682094471031");
			req.body(mtransotppayload).when();
			Response resp = req.post("https://test.equitybankgroup.com:8443/mfmbs/v1/otp/generate");
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("MFS M-Banking OTP Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS M-Banking OTP Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("MFS M-Banking OTP Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"MFS M-Banking OTP Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("MFS M-Banking OTP is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"MFS M-Banking OTP is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("MFS M-Banking OTP Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR, "MFS M-Banking OTP Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				break;
			case 400:
				logger.error("MFS M-Banking OTP Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS M-Banking OTP Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("MFS M-Banking OTP Response status line is" + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected MFS M-Banking OTP status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("status"), true,
					"MFS M-Banking OTP Response body does not contain Status key in it.");
			String otpsobj = resp.jsonPath().getString("status");
			if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("true")) {
				logger.info("MFS M-Banking OTP response is :  " + resp.body().asString());
				logger.info("MFS M-Banking OTP Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "MFS M-Banking OTP response is :  " + resp.body().asString()
						+ " and MFS M-Banking OTP Response Time is : " + resp.getTime());
			} else {
				logger.error("MFS M-Banking OTP status is either null, empty or false. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL, "MFS M-Banking OTP status is either null, empty or false.  " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		} else {
			logger.error("MFS Session ID is either null or empty.");
			exlogger.log(LogStatus.FAIL, "MFS Session ID is either null or empty.");
		}

	}

	@Test(enabled = true, priority = 11)
	public void paytocreditcards() {
		if (!sessionid.equalsIgnoreCase("null") && !sessionid.isEmpty()) {
			Map<String, Object> creditcardppayload = new HashMap<>();
			creditcardppayload.put("rid", "1");
			creditcardppayload.put("pin", "1234");
			creditcardppayload.put("contractNumber", "234214234234");
			creditcardppayload.put("token", credittoken);
			creditcardppayload.put("amount", "1200");
			creditcardppayload.put("sourceAccount", "242354290345234");
			creditcardppayload.put("cardType", "CREDIT");

			RequestSpecification req = RestAssured.given();
			req.header("Content-Type", "application/json");
			req.header("X_SESSION", sessionid);
			req.body(creditcardppayload).when();
			Response resp = req.post("https://test.equitybankgroup.com:8443/mfmbs/payToCreditCard");
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("MFS Pay to Credit Card Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Pay to Credit Card Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("MFS Pay to Credit Card Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"MFS Pay to Credit Card Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("MFS Pay to Credit Card is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"MFS Pay to Credit Card is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("MFS Pay to Credit Card Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR, "MFS M-Banking OTP Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				break;
			case 400:
				logger.error("MFS Pay to Credit Card Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Pay to Credit Card Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("MFS Pay to Credit Card Response status line is " + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected MFS Pay to Credit Card status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("description"), true,
					"MFS Pay to Credit Card Response body does not contain Status key in it.");
			String otpsobj = resp.jsonPath().getString("description");
			if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("success")) {
				logger.info("MFS Pay to Credit Card Success response is :  " + resp.body().asString());
				logger.info("MFS Pay to Credit Card Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "MFS M-Banking OTP Success response is :  " + resp.body().asString()
						+ " and MFS Pay to Credit Card Response Time is : " + resp.getTime());
			} else {
				logger.error("MFS Pay to Credit Card status is either null, empty or Fail. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL, "MFS Pay to Credit Card status is either null, empty or Fail.  " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		} else {
			logger.error("MFS Session ID is either null or empty.");
			exlogger.log(LogStatus.FAIL, "MFS Session ID is either null or empty.");
		}

	}

	@Test(enabled = true, priority = 12)
	public void geteazzyloan() {
		if (!sessionid.equalsIgnoreCase("null") && !sessionid.isEmpty()) {
			Map<String, Object> creditcardppayload = new HashMap<>();
			creditcardppayload.put("rid", "1");
			creditcardppayload.put("pin", "1234");
			creditcardppayload.put("account", "0350191765307");
			creditcardppayload.put("amount", "1200");

			RequestSpecification req = RestAssured.given();
			req.header("Content-Type", "application/json");
			req.header("X_SESSION", sessionid);
			req.body(creditcardppayload).when();
			Response resp = req.post("	https://test.equitybankgroup.com:8443/mfmbs/getLoan");
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("MFS Get Eazzy Loan Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Get Eazzy Loan Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("MFS Get Eazzy Loan Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"MFS Get Eazzy Loan Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("MFS Get Eazzy Loan is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"MFS Get Eazzy Loan is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("MFS Get Eazzy Loan Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR, "MFS Get Eazzy Loan Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				break;
			case 400:
				logger.error("MFS Get Eazzy Loan Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Get Eazzy Loan Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("MFS Get Eazzy Loan Response status line is " + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected MFS Pay to Credit Card status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("rrn"), true,
					"MFS Get Eazzy Loan Response body does not contain Status key in it.");
			String otpsobj = resp.jsonPath().getString("rrn");
			if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("ok")) {
				logger.info("MFS Get Eazzy Loan Success response is :  " + resp.body().asString());
				logger.info("MFS Get Eazzy Loan Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "MFS Get Eazzy Loan Success response is :  " + resp.body().asString()
						+ " and MFS Get Eazzy Loan Response Time is : " + resp.getTime());
			} else {
				logger.error("MFS Get Eazzy Loan status is either null, empty or Fail. " + resp.body().asString());
				exlogger.log(LogStatus.INFO, "MFS Get Eazzy Loan status is either null, empty or Fail.  " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		} else {
			logger.error("MFS Session ID is either null or empty.");
			exlogger.log(LogStatus.FAIL, "MFS Session ID is either null or empty.");
		}
	}

	@Test(enabled = true, priority = 13)
	public void transfertoequity() {
		if (!sessionid.equalsIgnoreCase("null") && !sessionid.isEmpty()) {
			Map<String, Object> creditcardppayload = new HashMap<>();
			creditcardppayload.put("rid", "1");
			creditcardppayload.put("pin", "2758");
			creditcardppayload.put("src", "0350191765307");
			creditcardppayload.put("dst", "1100194708355");
			creditcardppayload.put("amount", "1200");

			RequestSpecification req = RestAssured.given();
			req.header("Content-Type", "application/json");
			req.header("X_SESSION", sessionid);
			req.body(creditcardppayload).when();
			Response resp = req.post("https://test.equitybankgroup.com:8443/mfmbs/transferToEquity");
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("MFS Transfer To Equity Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Transfer To Equity Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("MFS Transfer To Equity Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"MFS Transfer To Equity Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("MFS Transfer To Equity is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"MFS Transfer To Equity is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("MFS Transfer To Equity Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"MFS Transfer To Equity Response is throwing a Page/Server not found error: "
								+ resp.getStatusLine());
				break;
			case 400:
				logger.error("MFS Transfer To Equity Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"MFS Transfer To Equity Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("MFS Transfer To Equity Response status line is " + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected MFS Transfer To Equity status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("rrn"), true,
					"MFS Transfer To Equity Response body does not contain Status key in it.");
			String otpsobj = resp.jsonPath().getString("rrn");
			if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("ok")) {
				logger.info("MFS Transfer To Equity Success response is :  " + resp.body().asString());
				logger.info("MFS Transfer To Equity Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "MFS Transfer To Equity Success response is :  " + resp.body().asString()
						+ " and MFS Transfer To Equity Response Time is : " + resp.getTime());
			} else {
				logger.error("MFS Transfer To Equity status is either null, empty or Fail. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL, "MFS Transfer To Equity status is either null, empty or Fail.  " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		} else {
			logger.error("MFS Session ID is either null or empty.");
			exlogger.log(LogStatus.FAIL, "MFS Session ID is either null or empty.");
		}
	}

}
