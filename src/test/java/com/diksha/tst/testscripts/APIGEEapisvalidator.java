package com.diksha.tst.testscripts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@Listeners(com.dikshatech.tst.configuration.TestNGListeners.class)
public class APIGEEapisvalidator extends TestBase {

	
	public static final Logger logger = Logger.getLogger(APIGEEapisvalidator.class);
	static String token;
	static String  currentdate;
	Response signresponse;
	Response paymentsign;
	String requestId;
	public static final String MERCHANT_CODE = "7768616098";
	public static final String PRIVATE_KEY = "rachel_uat_key";
	String currmethodname;
	
	// APigee API's starts from here.

		@Test(enabled = true, priority = 0)
		public static void datemethod() {
			exlogger = report.startTest("APIGEE API Validation Report");
			test = exlogger.assignCategory("APIGEE API Validation");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date dateobj = new Date();
			currentdate = df.format(dateobj);
			logger.info("Date: " + currentdate);
		}

		@Test(enabled = true, priority = 1)
		public static void authToken() {

			Response resp = RestAssured.given().urlEncodingEnabled(false)
					.header("Authorization", "Basic cUJPbkJUWDFDNm9sMjBERE9HTWt3Yk9RM3EyQXBpd2k6aXdWUXNva2w5Skd0R0l5eQ==")
					.header("Content-Type", "application/x-www-form-urlencoded").param("username", MERCHANT_CODE)
					.param("password", "J8m5e1Q9dLLWXZnmyAFAyFghPvNzn0RT").post("https://uat.jengahq.io/identity/v2/token");
			logger.info(resp.getStatusLine());
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("APIGEE Token Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL, "APIGEE Token Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				break;
			case 500:
				logger.error("APIGEE Token Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"APIGEE Token Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("APIGEE Token Response is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"APIGEE Token Response is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("APIGEE Token Response is throwing a Page/Server not found error: " + resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"APIGEE Token Response is throwing a Page/Server not found error: " + resp.getStatusLine());
				break;
			case 400:
				logger.error("APIGEE Token Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL, "APIGEE Token Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				break;
			default:
				logger.info("APIGEE Token Response status line is" + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected Payment Signature status code not returned and the statusline is: " + resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("access_token"), true,
					"Auth Token Response body does not contain access_token key in it.");
			token = resp.jsonPath().get("access_token");
			if (token != null && !token.equals("")) {
				try {
					logger.info("Access Token is : " + token + " and Token Response Time is : " + resp.getTime());
					exlogger.log(LogStatus.PASS, "Access token for Jenga API(APIGEE) is successfully generated : " + "["
							+ token + "]" + " and Token Response Time is : " + resp.getTime());
				} catch (Exception e) {
					logger.error("Access token is either null or empty " + "[" + token + "]");
					exlogger.log(LogStatus.FAIL,
							"Access token for Jenga API(APIGEE) is either NULL or Empty " + "[" + token + "]");
				}
			}

		}

		public String paymentsignature(String signdata, String methodname) {
			StringBuilder sb = new StringBuilder();
			String possible = "ABCDEFJHIJ0123456789";
			for (int i = 0; i < 12; i++) {
				int index = (int) (possible.length() * Math.random());
				sb.append(possible.charAt(index));
			}
			requestId = sb.toString();
			String plaintxt = "";
			switch (methodname) {
			case "creditscoreAPIGEE":
				plaintxt = signdata;
				break;
			case "createpaymentAPIGEE":
				plaintxt = requestId + signdata;
				break;
			case "tillpaymentAPIGEE":
				plaintxt = signdata + requestId;
				break;
			case "sendmoneyIFT":
				plaintxt = signdata + requestId;
				break;
			default:
				logger.error("No Such method defined");
				break;
			}
			String url = "http://40.127.183.157:8081/api/sign?private_key_name=" + PRIVATE_KEY + "&plain_text=" + plaintxt;
			logger.info("APIGEE Payment Signature URL " + url);
			paymentsign = RestAssured.given().get(url);
			switch (paymentsign.getStatusCode()) {
			case 504:
				logger.error("APIGEE Payment Signature Response is throwing a Gateway Timeout/Service Callout error:  "
						+ paymentsign.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"APIGEE Payment Signature Response is throwing a Gateway Timeout/Service Callout error:  "
								+ paymentsign.getStatusLine());
				break;
			case 500:
				logger.error("APIGEE Payment Signature Response is throwing an Internal Server error/Server side error: "
						+ paymentsign.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"APIGEE Payment Signature Response is throwing an Internal Server error/Server side error:  "
								+ paymentsign.getStatusLine());
				break;
			case 401:
				logger.error("APIGEE Token Response is throwing an Unauthorized error/invalid credentials: "
						+ paymentsign.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"APIGEE Token Response is throwing an Unauthorized error/invalid credentials: "
								+ paymentsign.getStatusLine());
				break;
			case 404:
				logger.error("APIGEE Payment Signature Response is throwing a Page/Server not found error: "
						+ paymentsign.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"APIGEE Payment Signature Response is throwing a Page/Server not found error: "
								+ paymentsign.getStatusLine());
				break;
			case 400:
				logger.error("APIGEE Payment Signature Response is throwing a Bad request/resource not found error: "
						+ paymentsign.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"APIGEE Payment Signature Response is throwing a Bad request/resource not found error: "
								+ paymentsign.getStatusLine());
				break;
			default:
				logger.info("APIGEE Payment Signature Response status line is " + paymentsign.getStatusLine());
				break;
			}
			Assert.assertEquals(paymentsign.getStatusCode(), 200,
					"Expected Payment Signature status code not returned and the statusline is: "
							+ paymentsign.getStatusLine());
			Assert.assertEquals(paymentsign.body().asString().contains("signature"), true,
					"APIGEE Signature Response body does not contain signature key in it.");
			JsonPath jsonPathEvaluator = paymentsign.jsonPath();
			String signresp = jsonPathEvaluator.get("signature");

			if (signresp != null && !signresp.equals("")) {
				logger.info("Signature response in string " + signresp);
				logger.info("Payment Signature Response Time : " + paymentsign.getTime());
				exlogger.log(LogStatus.INFO,
						"Payment Signature for " + methodname + " (Jenga) API is successfully generated : " + "[" + signresp
								+ "]" + " and Signature Response Time is : " + paymentsign.getTime());
				return signresp;
			} else {
				logger.error("Signature is either null or empty " + signresp);
				exlogger.log(LogStatus.FAIL, "Payment Signature for " + methodname + " (Jenga) API either null or empty "
						+ "[" + signresp + "]" + " and Response Time is : " + paymentsign.getTime());
			}
			return signresp;
		}

		@Test(enabled = true, priority = 2)
		public void creditscoreAPIGEE() {
			currmethodname = new Object() {
			}.getClass().getEnclosingMethod().getName();

			String dob = "1999-01-31";
			String merchantcode = MERCHANT_CODE;
			String docnumber = "12365478";

			String signformat = dob + merchantcode + docnumber;
			String signresp = paymentsignature(signformat, currmethodname);
			String creditscorepayload = "{\r\n" + "    \"customer\": [{\r\n" + "        \"id\": \"\",\r\n"
					+ "        \"fullName\": \"\",\r\n" + "        \"firstName\": \"\",\r\n"
					+ "        \"lastName\": \"\",\r\n" + "        \"shortName\": \"\",\r\n"
					+ "        \"title\": \"\",\r\n" + "        \"mobileNumber\": \"\",\r\n" + "        \"dateOfBirth\": \""
					+ dob + "\",\r\n" + "        \"identityDocument\": {\r\n"
					+ "            \"documentType\": \"NationalID\",\r\n" + "            \"documentNumber\": \"" + docnumber
					+ "\"\r\n" + "        }\r\n" + "    }],\r\n" + "    \"bureau\": {\r\n"
					+ "        \"reportType\": \"Mobile\",\r\n" + "        \"countryCode\": \"KE\"\r\n" + "    },\r\n"
					+ "    \"loan\": {\r\n" + "        \"amount\": \"5000\"\r\n" + "    }\r\n" + "}";

			RequestSpecification req = RestAssured.given();
			req.header("Authorization", "Bearer " + token);
			req.header("Content-Type", "application/json");
			req.header("signature", signresp);
			req.body(creditscorepayload).when();
			Response resp = req.post("https://uat.jengahq.io/customer/v2/creditinfo");
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("Credit Score APIGEE Response is throwing a Gateway Timeout:  " + resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Credit Score APIGEE Response is throwing a Gateway Timeout:  " + resp.getStatusLine());
				break;
			case 500:
				logger.error("Credit Score APIGEE Response is throwing an Internal Server error/Service Callout error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"Credit Score APIGEE Response is throwing an Internal Server error/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("Credit Score Response is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"Credit Score Response is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error(
						"Credit Score APIGEE Response is throwing a Page/Server not found error: " + resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"Credit Score APIGEE Response is throwing a Page/Server not found error: " + resp.getStatusLine());
				break;
			case 400:
				logger.error("Credit Score APIGEE Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Credit Score APIGEE Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected APIGEE Credit Score status code not returned and the statusline is: " + resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("CreditAccountsSummary"), true,
					"APIGEE Credit Score Response body does not contain CreditAccountsSummary key in it.");
			Map<String, String> creditsummary = resp.jsonPath().getMap("CreditAccountsSummary[0]");

			if (creditsummary != null && !creditsummary.isEmpty()) {
				logger.info("Credit Summary for Credit Score is :  " + creditsummary);
				logger.info("Credit Score Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "Credit Summary for Credit Score is :  " + creditsummary
						+ " and Credit Score Response Time is : " + resp.getTime());
			} else {
				logger.error("Credit Summary is either null or empty " + resp.body().asString());
				exlogger.log(LogStatus.FAIL,
						"Credit Summary for " + currmethodname + " (Jenga) API is either null or empty " + "["
								+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		}

		@Test(enabled = true, priority = 3)
		public void sendmoneyIFT() {
			currmethodname = new Object() {
			}.getClass().getEnclosingMethod().getName();

			String srcaccnt = "0740161904311";
			String transcurr = "KES";
			String transamount = "100";

			String signformat = srcaccnt + transamount + transcurr;
			String signaresp = paymentsignature(signformat, currmethodname);
			Map<String, Object> data = new HashMap<String, Object>();

			Map<String, Object> src = new HashMap<>();
			Map<String, Object> dest = new HashMap<>();
			Map<String, Object> trans = new HashMap<>();

			src.put("countryCode", "KE");
			src.put("name", "John Doe");
			src.put("accountNumber", srcaccnt);

			dest.put("type", "bank");
			dest.put("countryCode", "KE");
			dest.put("name", "Tom Doe");
			dest.put("cardNumber", "");
			dest.put("bankCode", "70");
			dest.put("bankBic", "");
			dest.put("accountNumber", "1100194907396");
			dest.put("mobileNumber", "");
			dest.put("walletName", "");

			trans.put("type", "InternalFundsTransfer");
			trans.put("amount", transamount);
			trans.put("currencyCode", transcurr);
			trans.put("reference", requestId);
			trans.put("date", currentdate);
			trans.put("description", "IFT Money Transfer test");

			data.put("source", src);
			data.put("destination", dest);
			data.put("transfer", trans);

			RequestSpecification req = RestAssured.given();
			req.header("Authorization", "Bearer " + token);
			req.header("Content-Type", "application/json");
			req.header("signature", signaresp);
			req.body(data).when();
			Response resp = req.post("https://uat.jengahq.io/transaction/v2/remittance");
			JsonPath jsonPathEvaluator = resp.jsonPath();
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("Send Money IFT APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Send Money IFT APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("Send Money IFT APIGEE Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"Send Money IFT APIGEE Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("Send Money IFT Response is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"Send Money IFT Response is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("Send Money IFT APIGEE Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR, "Send Money IFT APIGEE Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				break;
			case 400:
				logger.error("Send Money IFT APIGEE Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Send Money IFT APIGEE Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("Send Money IFT Response status line is" + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected APIGEE Send Money IFT status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("status"), true,
					"APIGEE Send Money IFT  Response body does not contain Status key in it.");

			String sendmonift = jsonPathEvaluator.get("status");
			if (sendmonift != null && !sendmonift.equals("") && sendmonift.equalsIgnoreCase("Success")) {
				logger.info("Status for Send Money IFT is :  " + sendmonift);
				logger.info("Send Money IFT Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "Status for Send Money IFT is :  " + sendmonift
						+ " and Send Money IFT Response Time is : " + resp.getTime());
			} else {
				logger.error("Status for Send Money IFT is either null, empty or Fail. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL, "Status for " + currmethodname + " (Jenga) API is either null or empty " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		}

		@Test(enabled = true, priority = 4)
		public void createpaymentAPIGEE() {
			currmethodname = new Object() {
			}.getClass().getEnclosingMethod().getName();

			String tranamount = "2000.00";
			String merchantcode = MERCHANT_CODE;
			String countrycode = "KE";

			String signformat = tranamount + merchantcode + countrycode;
			String signresp = paymentsignature(signformat, currmethodname);
			String tillpaymentpayload = "{\r\n" + "   \"customer\": {\r\n" + "      \"mobileNumber\": \"0764555320\",\r\n"
					+ "      \"countryCode\": \"" + countrycode + "\"\r\n" + "   },\r\n" + "   \"transaction\": {\r\n"
					+ "      \"amount\": \"" + tranamount + "\",\r\n"
					+ "      \"description\": \"A short description\",\r\n" + "      \"type\": \"exampleType\",\r\n"
					+ "      \"reference\": \"" + requestId + "\"\r\n" + "   }\r\n" + "}";

			RequestSpecification req = RestAssured.given();
			req.header("Authorization", "Bearer " + token);
			req.header("Content-Type", "application/json");
			req.header("signature", signresp);
			req.body(tillpaymentpayload).when();
			Response resp = req.post("https://uat.jengahq.io/transaction/v2/payments");
			JsonPath jsonPathEvaluator = resp.jsonPath();
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("Create Payment APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Create Payment APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("Create Payment APIGEE Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"Create Payment APIGEE Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("Credit Payment Response is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"Credit Payment Response is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("Create Payment APIGEE Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR, "Create Payment APIGEE Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				break;
			case 400:
				logger.error("Credit Payment APIGEE Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Credit Payment APIGEE Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("Create Payment Response status line is" + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected APIGEE Send Money IFT status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("status"), true,
					"APIGEE Send Money IFT  Response body does not contain Status key in it.");

			String createpay = jsonPathEvaluator.get("status");
			if (createpay != null && !createpay.equals("") && createpay.equalsIgnoreCase("Success")) {
				logger.info("Status for Create Payment is :  " + createpay);
				logger.info("Create Payment Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "Status for Create Payment is :  " + createpay
						+ " and Create Payment Response Time is : " + resp.getTime());
			} else {
				logger.error("Status for Create Payment is either null, empty or Fail. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL, "Status for " + currmethodname + " (Jenga) API is either null or empty " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		}

		@Test(enabled = true, priority = 5)
		public void tillpaymentAPIGEE() {
			currmethodname = new Object() {
			}.getClass().getEnclosingMethod().getName();
			String merchtill = "0766555084";
			String partnerid = "0740161904311";
			String payamount = "1000.00";
			String paycurr = "KES";

			String signformat = merchtill + partnerid + payamount + paycurr;
			String signresp = paymentsignature(signformat, currmethodname);
			String tillpaymentpayload = "{\r\n" + "  \"merchant\": {\r\n" + "    \"till\": \"" + merchtill + "\"\r\n"
					+ "  },\r\n" + "  \"payment\": {\r\n" + "    \"ref\": \"" + requestId + "\",\r\n" + "    \"amount\": \""
					+ payamount + "\",\r\n" + "    \"currency\": \"" + paycurr + "\"\r\n" + "  },\r\n"
					+ "  \"partner\": {\r\n" + "    \"id\": \"" + partnerid + "\",\r\n" + "    \"ref\": \"" + requestId
					+ "\"\r\n" + "  }\r\n" + "}";

			RequestSpecification req = RestAssured.given();
			req.header("Authorization", "Bearer " + token);
			req.header("Content-Type", "application/json");
			req.header("signature", signresp);
			req.body(tillpaymentpayload).when();
			Response resp = req.post("https://uat.jengahq.io/transaction/v2/tills/pay");
			JsonPath jsonPathEvaluator = resp.jsonPath();
			switch (resp.getStatusCode()) {
			case 504:
				logger.error("Till Payment APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Till Payment APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("Till Payment APIGEE Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"Till Payment APIGEE Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("Till Payment Response is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"Till Payment Response is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error(
						"Till Payment APIGEE Response is throwing a Page/Server not found error: " + resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"Till Payment APIGEE Response is throwing a Page/Server not found error: " + resp.getStatusLine());
				break;
			case 400:
				logger.error("Till Payment APIGEE Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Till Payment APIGEE Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("Till Payment Response status line is" + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected APIGEE Till Payment status code not returned and the statusline is: " + resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("status"), true,
					"APIGEE Till Payment Response body does not contain Status key in it.");

			String tillpay = jsonPathEvaluator.get("status");
			if (tillpay != null && !tillpay.equals("") && tillpay.equalsIgnoreCase("Success")) {
				logger.info("Status for Till Payment is :  " + tillpay);
				logger.info("Till Payment Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "Status for Till Payment is :  " + tillpay
						+ " and Till Payment Response Time is : " + resp.getTime());
				exlogger.log(LogStatus.INFO, "Till Payment Success Response body  :  " + resp.body().asString());
			} else {
				logger.error("Status for Till Payment is either null, empty or Fail. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL, "Status for " + currmethodname + " (Jenga) API is either null or empty " + "["
						+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}
		}

		@Test(enabled = true, priority = 7)
		public void billvalidationAPIGEE() {
			Map<String, Object> billppayload = new HashMap<String, Object>();
			billppayload.put("billerCode", "123456");
			billppayload.put("customerRefNumber", "1234567890123");
			billppayload.put("amount", "1000.00");
			billppayload.put("amountCurrency", "KES");

			RequestSpecification req = RestAssured.given();
			req.header("Authorization", "Bearer " + token);
			req.header("Content-Type", "application/json");
			req.body(billppayload).when();
			Response resp = req.post("https://uat.jengahq.io/transaction/v2/bills/validation");
			JsonPath jsonPathEvaluator = resp.jsonPath();
			switch (resp.getStatusCode()) {
			case 504:
				logger.error(
						"Bill Validation Payment APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Bill Validation APIGEE Response is throwing a Gateway Timeout/Service Callout error:  "
								+ resp.getStatusLine());
				break;
			case 500:
				logger.error("Bill Validation APIGEE Response is throwing an Internal Server error/Server side error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.UNKNOWN,
						"Bill Validation APIGEE Response is throwing an Internal Server error/Server side error:  "
								+ resp.getStatusLine());
				break;
			case 401:
				logger.error("Bill Validation Response is throwing an Unauthorized error/invalid credentials: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR,
						"Bill Validation Response is throwing an Unauthorized error/invalid credentials: "
								+ resp.getStatusLine());
				break;
			case 404:
				logger.error("Bill Validation APIGEE Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.ERROR, "Bill Validation APIGEE Response is throwing a Page/Server not found error: "
						+ resp.getStatusLine());
				break;
			case 400:
				logger.error("Bill Validation APIGEE Response is throwing a Bad request/resource not found error: "
						+ resp.getStatusLine());
				exlogger.log(LogStatus.FATAL,
						"Bill Validation APIGEE Response is throwing a Bad request/resource not found error: "
								+ resp.getStatusLine());
				break;
			default:
				logger.info("Bill Validation Response status line is" + resp.getStatusLine());
				break;
			}
			Assert.assertEquals(resp.getStatusCode(), 200,
					"Expected APIGEE Bill Validation status code not returned and the statusline is: "
							+ resp.getStatusLine());
			Assert.assertEquals(resp.body().asString().contains("status"), true,
					"APIGEE Bill Validation Response body does not contain Status key in it.");

			String billvalidation = jsonPathEvaluator.get("bill.message");
			if (billvalidation != null && !billvalidation.isEmpty() && billvalidation.equalsIgnoreCase("Success")) {
				logger.info("Status message for Bill Validation is :  " + billvalidation);
				logger.info("Bill Validation Response Time : " + resp.getTime());
				exlogger.log(LogStatus.PASS, "Status message for Bill Validation  is :  " + billvalidation
						+ " and Bill Validation Response Time is : " + resp.getTime());
				exlogger.log(LogStatus.INFO, "Bill Validation Success Response body :  " + resp.body().asString());
			} else {
				logger.error("Status message for Bill Validation is either null, empty or Fail. " + resp.body().asString());
				exlogger.log(LogStatus.FAIL,
						"Status message for " + currmethodname + " (Jenga) API is either null or empty " + "["
								+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
			}

		}

}
