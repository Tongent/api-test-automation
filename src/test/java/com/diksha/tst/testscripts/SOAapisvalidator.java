package com.diksha.tst.testscripts;

import java.io.File;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.dikshatech.tst.configuration.ConfigDataProvider;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@Listeners(com.dikshatech.tst.configuration.TestNGListeners.class)
public class SOAapisvalidator extends TestBase {
	
	public static final Logger logger = Logger.getLogger(SOAapisvalidator.class);

	@Test(enabled = true, priority = 14)
	public static void setEndpointURL() {
		exlogger = report.startTest("SOA API Validation Report");
		test = exlogger.assignCategory("SOA API Validation");
		RestAssured.urlEncodingEnabled = false;
		RestAssured.authentication = RestAssured.preemptive().basic("test", "test123");
	}

	@Test(enabled = true, priority = 15)
	public static void accountcurrency() {

		File payload = new File(ConfigDataProvider.getProp().getProperty("accntcurrxmlpath"));

		Response resp = RestAssured.given().contentType(ContentType.XML).accept(ContentType.JSON).body(payload).when()
				.post("https://wsuat.equitybankgroup.com/ESB/PS/Finacle/REST/Account/queryAccountCurrency");

		switch (resp.getStatusCode()) {
		case 504:
			logger.error("SOA Account Currency Response is throwing a Gateway Timeout/Service Callout error:  "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Account Currency Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			break;
		case 500:
			logger.error("SOA Account Currency Response is throwing an Internal Server error/Server side error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.UNKNOWN,
					"SOA Account Currency Response is throwing an Internal Server error/Server side error:  "
							+ resp.getStatusLine());
			break;
		case 401:
			logger.error("SOA Account Currency is throwing an Unauthorized error/invalid credentials: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR, "SOA Account Currency is throwing an Unauthorized error/invalid credentials: "
					+ resp.getStatusLine());
			break;
		case 404:
			logger.error(
					"SOA Account Currency Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Account Currency Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			break;
		case 400:
			logger.error("SOA Account Currency Response is throwing a Bad request/resource not found error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Account Currency Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			break;
		default:
			logger.info("SOA Account Currency Response status line is " + resp.getStatusLine());
			break;
		}
		Assert.assertEquals(resp.getStatusCode(), 200,
				"Expected SOA Account Currency status code not returned and the statusline is: "
						+ resp.getStatusLine());
		Assert.assertEquals(resp.body().asString().contains("RsData"), true,
				"SOA Account Currency Response body does not contain Status key in it.");
		String otpsobj = resp.jsonPath().getString("RsData.Status.Type");
		String acccurrobj = resp.jsonPath().getString("RsData.Account.AccountIdentifier.AccountCurrency");
		if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("[Success]")) {
			logger.info("The returned currency is :" + acccurrobj);
			logger.info("SOA Account Currency Success response is :  " + resp.body().asString());
			logger.info("SOA Account Currency Response Time : " + resp.getTime());
			exlogger.log(LogStatus.INFO, "The returned currency is :" + acccurrobj);
			exlogger.log(LogStatus.PASS, "SOA Account Currency Success response is :  " + resp.body().asString()
					+ " and SOA Account Currency Response Time is : " + resp.getTime());
		} else {
			logger.error("SOA Account Currency status is either null, empty or Fail. " + resp.body().asString());
			exlogger.log(LogStatus.FAIL, "SOA Account Currency status is either null, empty or Fail.  " + "["
					+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
		}

	}

	@Test(enabled = true, priority = 16)
	public static void loancreation() {

		File payload = new File(ConfigDataProvider.getProp().getProperty("loancreationxmlpath"));

		Response resp = RestAssured.given().contentType(ContentType.XML).accept(ContentType.JSON).body(payload).when()
				.post("https://wsuat.equitybankgroup.com/ESB/PS/Finacle/V1/REST/Account/custAcctInquiry");

		switch (resp.getStatusCode()) {
		case 504:
			logger.error("SOA Loan Creation Response is throwing a Gateway Timeout/Service Callout error:  "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Loan Creation Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			break;
		case 500:
			logger.error("SOA Loan Creation Response is throwing an Internal Server error/Server side error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.UNKNOWN,
					"SOA Loan Creation Response is throwing an Internal Server error/Server side error:  "
							+ resp.getStatusLine());
			break;
		case 401:
			logger.error(
					"SOA Loan Creation is throwing an Unauthorized error/invalid credentials: " + resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Loan Creation is throwing an Unauthorized error/invalid credentials: " + resp.getStatusLine());
			break;
		case 404:
			logger.error(
					"SOA Loan Creation Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Loan Currency Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			break;
		case 400:
			logger.error("SOA Loan Creation Response is throwing a Bad request/resource not found error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Loan Creation Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			break;
		default:
			logger.info("SOA Loan Creation Response status line is " + resp.getStatusLine());
			break;
		}
		Assert.assertEquals(resp.getStatusCode(), 200,
				"Expected SOA Loan Creation status code not returned and the statusline is: " + resp.getStatusLine());
		Assert.assertEquals(resp.body().asString().contains("RsData"), true,
				"SOA Loan Creation Response body does not contain Status key in it.");
		String otpsobj = resp.jsonPath().getString("RsData.Status.Type");
		String cifobj = resp.jsonPath().getString("RsData.Account.Customer.CustomerIdentifier.CustomerID");
		if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("[DUP:SUCCESS]")) {
			logger.info("The returned Customer ID is :" + cifobj);
			logger.info("SOA Loan Creation Success response is :  " + resp.body().asString());
			logger.info("SOA Loan Creation Response Time : " + resp.getTime());
			exlogger.log(LogStatus.INFO, "The returned Customer ID is :" + cifobj);
			exlogger.log(LogStatus.PASS, "SOA Loan Creation Success response is :  " + resp.body().asString()
					+ " and SOA Loan Creation Response Time is : " + resp.getTime());
		} else {
			logger.error("SOA Loan Creation status is either null, empty or Fail. " + resp.body().asString());
			exlogger.log(LogStatus.FAIL, "SOA Loan Creation status is either null, empty or Fail.  " + "["
					+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
		}

	}

	@Test(enabled = true, priority = 17)
	public static void custaccntenqcif() {

		File payload = new File(ConfigDataProvider.getProp().getProperty("custaccenqCIFxmlpath"));

		Response resp = RestAssured.given().contentType(ContentType.XML).accept(ContentType.JSON).body(payload).when()
				.post("https://wsuat.equitybankgroup.com/ESB/PS/Finacle/V1/REST/Account/custAcctInquiry");

		switch (resp.getStatusCode()) {
		case 504:
			logger.error(
					"SOA Customer Account Enquiry -CIF Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Customer Account Enquiry -CIF Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			break;
		case 500:
			logger.error(
					"SOA Customer Account Enquiry -CIF Response is throwing an Internal Server error/Server side error: "
							+ resp.getStatusLine());
			exlogger.log(LogStatus.UNKNOWN,
					"SOA Customer Account Enquiry -CIF Response is throwing an Internal Server error/Server side error:  "
							+ resp.getStatusLine());
			break;
		case 401:
			logger.error("SOA Customer Account Enquiry -CIF is throwing an Unauthorized error/invalid credentials: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Customer Account Enquiry -CIF is throwing an Unauthorized error/invalid credentials: "
							+ resp.getStatusLine());
			break;
		case 404:
			logger.error("SOA Customer Account Enquiry -CIF Response is throwing a Page/Server not found error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Loan Currency Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			break;
		case 400:
			logger.error(
					"SOA Customer Account Enquiry -CIF Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Customer Account Enquiry -CIF Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			break;
		default:
			logger.info("SOA Customer Account Enquiry -CIF Response status line is " + resp.getStatusLine());
			break;
		}
		Assert.assertEquals(resp.getStatusCode(), 200,
				"Expected SOA Customer Account Enquiry -CIF status code not returned and the statusline is: "
						+ resp.getStatusLine());
		Assert.assertEquals(resp.body().asString().contains("RsData"), true,
				"SOA Customer Account Enquiry -CIF Response body does not contain Status key in it.");
		String otpsobj = resp.jsonPath().getString("RsData.Status.Type");
		String cifobj = resp.jsonPath().getString("RsData.Account.Customer.CustomerIdentifier.CustomerID");
		if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("[DUP:SUCCESS]")) {
			logger.info("The returned Customer ID is :" + cifobj);
			logger.info("SOA Customer Account Enquiry -CIF Success response is :  " + resp.body().asString());
			logger.info("SOA Customer Account Enquiry -CIF Response Time : " + resp.getTime());
			exlogger.log(LogStatus.INFO, "The returned Customer ID is :" + cifobj);
			exlogger.log(LogStatus.PASS,
					"SOA Customer Account Enquiry -CIF Success response is :  " + resp.body().asString()
							+ " and SOA Customer Account Enquiry -CIF Response Time is : " + resp.getTime());
		} else {
			logger.error("SOA Customer Account Enquiry -CIF status is either null, empty or Fail. "
					+ resp.body().asString());
			exlogger.log(LogStatus.FAIL, "SOA Customer Account Enquiry -CIF status is either null, empty or Fail.  "
					+ "[" + resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
		}

	}

	@Test(enabled = true, priority = 18)
	public static void custaccntenqcid() {

		File payload = new File(ConfigDataProvider.getProp().getProperty("custaccenqIDxmlpath"));

		Response resp = RestAssured.given().contentType(ContentType.XML).accept(ContentType.JSON).body(payload).when()
				.post("https://wsuat.equitybankgroup.com/ESB/PS/Finacle/V1/REST/Account/custAcctInquiry");

		switch (resp.getStatusCode()) {
		case 504:
			logger.error(
					"SOA Customer Account Enquiry -ID Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Customer Account Enquiry -ID Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			break;
		case 500:
			logger.error(
					"SOA Customer Account Enquiry -ID Response is throwing an Internal Server error/Server side error: "
							+ resp.getStatusLine());
			exlogger.log(LogStatus.UNKNOWN,
					"SOA Customer Account Enquiry -ID Response is throwing an Internal Server error/Server side error:  "
							+ resp.getStatusLine());
			break;
		case 401:
			logger.error("SOA Customer Account Enquiry -ID is throwing an Unauthorized error/invalid credentials: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Customer Account Enquiry -ID is throwing an Unauthorized error/invalid credentials: "
							+ resp.getStatusLine());
			break;
		case 404:
			logger.error("SOA Customer Account Enquiry -ID Response is throwing a Page/Server not found error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Loan Currency Response is throwing a Page/Server not found error: " + resp.getStatusLine());
			break;
		case 400:
			logger.error(
					"SOA Customer Account Enquiry -ID Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Customer Account Enquiry -ID Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			break;
		default:
			logger.info("SOA Customer Account Enquiry -ID Response status line is " + resp.getStatusLine());
			break;
		}
		Assert.assertEquals(resp.getStatusCode(), 200,
				"Expected SOA Customer Account Enquiry -ID status code not returned and the statusline is: "
						+ resp.getStatusLine());
		Assert.assertEquals(resp.body().asString().contains("RsData"), true,
				"SOA Customer Account Enquiry -ID Response body does not contain Status key in it.");
		String otpsobj = resp.jsonPath().getString("RsData.Status.Type");
		String cifobj = resp.jsonPath().getString("RsData.Account.Customer.CustomerIdentifier.CustomerID");
		if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("[DUP:SUCCESS]")) {
			logger.info("The returned Customer ID is :" + cifobj);
			logger.info("SOA Customer Account Enquiry -ID Success response is :  " + resp.body().asString());
			logger.info("SOA Customer Account Enquiry -ID Response Time : " + resp.getTime());
			exlogger.log(LogStatus.INFO, "The returned Customer ID is :" + cifobj);
			exlogger.log(LogStatus.PASS,
					"SOA Customer Account Enquiry -ID Success response is :  " + resp.body().asString()
							+ " and SOA Customer Account Enquiry -ID Response Time is : " + resp.getTime());
		} else {
			logger.error(
					"SOA Customer Account Enquiry -ID status is either null, empty or Fail. " + resp.body().asString());
			exlogger.log(LogStatus.FAIL, "SOA Customer Account Enquiry -ID status is either null, empty or Fail.  "
					+ "[" + resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
		}

	}

	@Test(enabled = true, priority = 19)
	public static void currexchrate() {

		File payload = new File(ConfigDataProvider.getProp().getProperty("currexchnratexmlpath"));

		Response resp = RestAssured.given().contentType(ContentType.XML).accept(ContentType.XML).body(payload).when()
				.post("https://wsuat.equitybankgroup.com/ESB/PS/Finacle/REST/CurrencyExchange/onlineTest");

		switch (resp.getStatusCode()) {
		case 504:
			logger.error("SOA Currency Exchange Rate Response is throwing a Gateway Timeout/Service Callout error:  "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Currency Exchange Rate Response is throwing a Gateway Timeout/Service Callout error:  "
							+ resp.getStatusLine());
			break;
		case 500:
			logger.error("SOA Currency Exchange Rate Response is throwing an Internal Server error/Server side error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.UNKNOWN,
					"SOA Currency Exchange Rate Response is throwing an Internal Server error/Server side error:  "
							+ resp.getStatusLine());
			break;
		case 401:
			logger.error("SOA Currency Exchange Rate is throwing an Unauthorized error/invalid credentials: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Currency Exchange Rate is throwing an Unauthorized error/invalid credentials: "
							+ resp.getStatusLine());
			break;
		case 404:
			logger.error("SOA Currency Exchange Rate Response is throwing a Page/Server not found error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.ERROR,
					"SOA Currency Exchange Rate Response is throwing a Page/Server not found error: "
							+ resp.getStatusLine());
			break;
		case 400:
			logger.error("SOA Currency Exchange Rate Response is throwing a Bad request/resource not found error: "
					+ resp.getStatusLine());
			exlogger.log(LogStatus.FATAL,
					"SOA Currency Exchange Rate Response is throwing a Bad request/resource not found error: "
							+ resp.getStatusLine());
			break;
		default:
			logger.info("SOA Currency Exchange Rate Response status line is " + resp.getStatusLine());
			break;
		}
		Assert.assertEquals(resp.getStatusCode(), 200,
				"Expected SOA Currency Exchange Rate status code not returned and the statusline is: "
						+ resp.getStatusLine());
		Assert.assertEquals(resp.body().asString().contains("RsData"), true,
				"SOA Currency Exchange Rate Response body does not contain Status key in it.");
		String otpsobj = resp.xmlPath().getString("ESBMsg.RsData.Status.Type");
		String exchrateobj = resp.xmlPath().getString("ESBMsg.RsData.CurrencyExchange.ConversionRate");
		if (otpsobj != null && !otpsobj.isEmpty() && otpsobj.equalsIgnoreCase("DUP:SUCCESS")) {
			logger.info("The returned Currency Exchange rate is :" + exchrateobj);
			logger.info("SOA Currency Exchange Rate Success response is :  " + resp.body().asString());
			logger.info("SOA Currency Exchange Rate Response Time : " + resp.getTime());
			exlogger.log(LogStatus.INFO, "The returned Exchange Rate is :" + exchrateobj);
			exlogger.log(LogStatus.PASS, "SOA Currency Exchange Rate Success response is :  " + resp.body().asString()
					+ " and SOA Currency Exchange Rate Response Time is : " + resp.getTime());
		} else {
			logger.error("SOA Currency Exchange Rate status is either null, empty or Fail. " + resp.body().asString());
			exlogger.log(LogStatus.FAIL, "SOA Currency Exchange Rate status is either null, empty or Fail.  " + "["
					+ resp.body().asString() + "]" + " and Response Time is : " + resp.getTime());
		}

	}

}
