package com.diksha.tst.testscripts;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.dikshatech.tst.configuration.ConfigDataProvider;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class TestBase {

	public static ExtentReports report;
	public static ExtentTest exlogger;
	public static String reportConfigpath = new File(ConfigDataProvider.getProp().getProperty("configpath"))
			.getAbsolutePath();
	public static ExtentTest test;
	
	@BeforeClass(alwaysRun = true)
	public static void reportIntializer() {
		File testDirectory = new File(ConfigDataProvider.getProp().getProperty("loggerpath"));
		boolean result = false;
		if (!testDirectory.exists()) {
			System.out.println("creating directory: " + testDirectory.getName());

			try {
				testDirectory.createNewFile();
				result = true;
			} catch (SecurityException | IOException se) {
				System.out.println(se);
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		report = new ExtentReports(ConfigDataProvider.getProp().getProperty("loggerpath"), false,
				DisplayOrder.NEWEST_FIRST);
		report.addSystemInfo("Host Name", "Equity-EazzyBiz & Eazzy Bank").addSystemInfo("Environment", "UAT")
				.addSystemInfo("User Name", System.getProperty("user.name")).loadConfig(new File(reportConfigpath));
		
	}

	@AfterSuite(alwaysRun = true)
	public void extentreport() {

		report.endTest(exlogger);
		report.flush();
		report.close();

	}

}
