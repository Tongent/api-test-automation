package com.dikshatech.tst.configuration;

import java.lang.reflect.Method;

import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.ITestAnnotation;

import com.diksha.tst.testscripts.APIvalidator;
import com.relevantcodes.extentreports.LogStatus;

import java.lang.reflect.Constructor;

public class TestNGListeners extends APIvalidator implements ITestListener {

	public void onTestStart(ITestResult result) {
		logger.info("Testcase Started and testcase name is : " + result.getName());
		exlogger.log(LogStatus.INFO, "Testcase Started and Testcase name is : " + result.getName());
	}

	public void onTestSuccess(ITestResult result) {
		logger.info("Testcase verified and succeed and testcase name is : " + result.getName());
		exlogger.log(LogStatus.PASS, "Testcase verified and succeed and testcase name is : " + result.getName());
	}

	public void onTestFailure(ITestResult result) {
		logger.info("Testcase failed and Testcase name is : " + result.getName());
		exlogger.log(LogStatus.FAIL, "Testcase failed and testcase name is : " + result.getName() + "Failed due to - "
				+ result.getThrowable());
	}

	public void onTestSkipped(ITestResult result) {
		logger.info("Testcase skipped and testcase name is : " + result.getName());
		exlogger.log(LogStatus.SKIP, "Testcase skipped and Testcase name is : " + result.getName() + "Skipped due to - "
				+ result.getThrowable());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}

	public void onStart(ITestContext context) {
	}

	public void onFinish(ITestContext context) {
	}

	public void transform(ITestAnnotation testannotation, Class<?> testClass, Constructor<?> testConstructor,
			Method testMethod) {
		IRetryAnalyzer retry = testannotation.getRetryAnalyzer();
		if (retry == null) {
			// testannotation.setRetryAnalyzer(RetryFailedTestCases.class);
		}

	}

}