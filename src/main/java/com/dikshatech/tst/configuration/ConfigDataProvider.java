package com.dikshatech.tst.configuration;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigDataProvider{
	public static final Logger logger = Logger.getLogger(ConfigDataProvider.class);

	protected static  Properties prop;

	protected ConfigDataProvider() {}
	static {
		try {
			FileInputStream FIS = new FileInputStream("src/main/java/com/dikshatech/tst/configuration/config.properties");
			setProp(new Properties());
			getProp().load(FIS);
		} catch (Exception e) {
			logger.info("Exception is " + e.getMessage());
		}
	}

	public static String loginExcel() {
		return getProp().getProperty("logincredential");
	}

	public static String leavesExcel() {
		return getProp().getProperty("leavesheet");
	}

	public static String perdiemreportID() {
		return getProp().getProperty("perdiemreport_Id");
	}

	public static String getleaveUrl() {
		return getProp().getProperty("leaveurl");
	}

	public static String get(String key) {
		return getProp().getProperty(key);
	}

	public static Properties getProp() {
		return prop;
	}

	public static void setProp(Properties prop) {
		ConfigDataProvider.prop = prop;
	}
}